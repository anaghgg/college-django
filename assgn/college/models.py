from django.db import models

# Create your models here.
class College(models.Model):
    college_name=models.CharField(max_length=100,unique=True)
    college_address=models.CharField(max_length=500,null=True)
    def __str__(self):
        return self.college_name

class Student(models.Model):
    college_name=models.ForeignKey(College,on_delete=models.CASCADE)
    student_name=models.CharField(max_length=100)
    branch=models.CharField(max_length=100)
    first_year=models.IntegerField(null=True)
    second_year=models.IntegerField(null=True)
    third_year=models.IntegerField(null=True)
    fourth_year=models.IntegerField(null=True)

    def __str__(self):
        return self.student_name