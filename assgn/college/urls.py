from . import views
from django.urls import path

app_name = "college"

urlpatterns=[
    path('',views.index,name="colleges"),
    path('students/<int:college_id>/',views.students,name="students"),
]