from django.shortcuts import render
from .models import College,Student

# Create your views here.
def index(request):
    colleges=list(College.objects.all())
    context={'colleges' : colleges}
    return render(request,'college/index.html',context)
    
def students(request,college_id):
    college=College.objects.get(pk=college_id)
    college_students=list(Student.objects.filter(college_name=college))
    average_marks=[]
    for student in college_students:
        total_marks=student.first_year+student.second_year+student.third_year+student.fourth_year
        average=int(total_marks/4)
        average_marks.append(average)

    details=zip(college_students,average_marks)
    return render(request,'college/students.html',{'college':college,'details':details})
