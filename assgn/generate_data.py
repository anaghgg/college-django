from college.models import Student,College
from random import randint

colleges=list(College.objects.all())
branches=['IT','CS','EE','ECE','MECH','CIVIL']

#Create 100 records
#Length of student name= 3
for clg in colleges:
    for i in range(100):
        name=''
        for j in range(3):
            name=name+chr(randint(ord('a'),ord('z')))
        
        br=branches[randint(0,len(branches)-1)]
        
        marks=[]
        for i in range(4):
            marks.append(randint(50,99))

        student=Student(college_name=clg,student_name=name,branch=br,first_year=marks[0],second_year=marks[1],third_year=marks[2],fourth_year=marks[3])
        student.save()
